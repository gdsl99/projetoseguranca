package daw2.projetoescola.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class IndexController {

	private static final Logger logger = LoggerFactory.getLogger(IndexController.class);

	@GetMapping(value = {"/", "/index.html"} )
	public ModelAndView index() {
		logger.trace("Entrou em index");
		ModelAndView mv = new ModelAndView("index");
		logger.trace("Encaminhando para a view index");
		return mv;
	}
	
	@GetMapping(value = {"/login"} )
	public String login() {
		logger.trace("Entrou em login");
		logger.trace("Encaminhando para a view login");
		return "login";
	}
	
}