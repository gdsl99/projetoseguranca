package daw2.projetoescola.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/aluno")
public class AlunoController {

	private static final Logger logger = LoggerFactory.getLogger(AlunoController.class);

	@GetMapping("/cadastrar")
	public ModelAndView cadastrar() {
		logger.trace("Entrou em cadastrar");
        ModelAndView mv = new ModelAndView("mostrarmensagem");
        mv.addObject("mensagem", "Apenas o admin e secretaria podem acessar. Estrobilobaldo, Ana, João e Sheila podem ver isso aqui.");
        logger.trace("Encaminhando para a view mostrarmensagem");
        return mv;
	}
	
	@GetMapping("/buscar")
	public ModelAndView buscar() {
		logger.trace("Entrou em buscar");
		ModelAndView mv = new ModelAndView("mostrarmensagem");
		mv.addObject("mensagem", "Apenas o admin e secretaria podem acessar. Estrobilobaldo, Ana, João e Sheila podem ver isso aqui.");
        logger.trace("Encaminhando para a view mostrarmensagem");
        return mv;
	}
	
    @GetMapping("/remover")
    public ModelAndView remover() {
    	logger.trace("Entrou em remover");
    	ModelAndView mv = new ModelAndView("mostrarmensagem");
    	mv.addObject("mensagem", "Apenas o admin e secretaria podem acessar. Estrobilobaldo, Ana, João e Sheila podem ver isso aqui.");
        logger.trace("Encaminhando para a view mostrarmensagem");
        return mv;
    }
    
    @GetMapping("/alterar")
    public ModelAndView alterar() {
    	logger.trace("Entrou em alterar");
    	ModelAndView mv = new ModelAndView("mostrarmensagem");
    	mv.addObject("mensagem", "Apenas o admin e secretaria podem acessar. Estrobilobaldo, Ana, João e Sheila podem ver isso aqui.");
        logger.trace("Encaminhando para a view mostrarmensagem");
        return mv;
    }
    
    @GetMapping("/notas")
    public ModelAndView notas() {
    	logger.trace("Entrou em notas");
    	ModelAndView mv = new ModelAndView("mostrarmensagem");
        mv.addObject("mensagem", "Apenas o admin e professor podem acessar. Estrobilobaldo, Ana, Jorge e Grosbilda podem ver isso aqui.");
        logger.trace("Encaminhando para a view mostrarmensagem");
        return mv;
    }
    
    @GetMapping("/relatorio")
    public ModelAndView relatorio() {
    	logger.trace("Entrou em relatorio");
    	ModelAndView mv = new ModelAndView("mostrarmensagem");
        mv.addObject("mensagem", "Você deve alterar essa funcionalidade para que gere o relatório em PDF. Apenas o admin pode acessar. Só o Estrobilobaldo pode ver isso aqui.");
        logger.trace("Encaminhando para a view mostrarmensagem");
        return mv;
    }
	
}








